package info.jntuacea.jntuacea;

import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.BufferedInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.Map;

public class DisplayBooksActivity extends AppCompatActivity {

    private String branch,semester;
    private StorageReference storageReference;
    private DatabaseReference databaseReference;
    private ProgressDialog mProgressDialog;
    private Uri bookUri;
    private String nameOfBook;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_books);
        branch = getIntent().getStringExtra("branch");
        semester = getIntent().getStringExtra("semester");
        storageReference = FirebaseStorage.getInstance().getReference();
        databaseReference = FirebaseDatabase.getInstance().getReference();

        final LinearLayout ll = (LinearLayout) findViewById(R.id.activity_display_books);
        final LayoutParams lp = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);

        final ProgressDialog pd = new ProgressDialog(DisplayBooksActivity.this);
        pd.setMessage("Retrieving Books ....");
        pd.show();

        databaseReference.child(branch).child(semester).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Map<String,String> books = (Map<String,String>) dataSnapshot.getValue();
                for(Map.Entry<String,String> entry : books.entrySet()) {
                    Log.d("test",entry.getKey() + " " + entry.getValue());
                    final String bookName = entry.getValue();
                    LayoutInflater lf = getLayoutInflater();
                    final LinearLayout book = (LinearLayout) lf.inflate(R.layout.book,null);
                    ((TextView) book.findViewById(R.id.book_name)).setText(entry.getKey());

                    book.findViewById(R.id.view_book).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            storageReference.child(branch).child(semester).child(bookName).getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                @Override
                                public void onSuccess(Uri uri) {
                                    bookUri = uri;
                                    if(isStoragePermissionGranted(0)) {
                                        new DownloadFile().execute(uri.toString());
                                    }
                                }
                            }).addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception exception) {
                                    // Handle any errors
                                }
                            });
                        }
                    });

                    book.findViewById(R.id.download_book).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            storageReference.child(branch).child(semester).child(bookName).getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                @Override
                                public void onSuccess(Uri uri) {
                                    bookUri = uri;
                                    nameOfBook = bookName;
                                    if(isStoragePermissionGranted(1)) {
                                        downloadFileForViewing(uri,bookName);
                                    }
                                }
                            }).addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception exception) {
                                    // Handle any errors
                                }
                            });
                        }
                    });

                    ll.addView(book);
                }
                pd.dismiss();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }


    public void downloadFileForViewing(Uri uri,String bookName) {
        DownloadManager.Request r = new DownloadManager.Request(uri);

        r.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, bookName);
        r.allowScanningByMediaScanner();
        r.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);

        DownloadManager dm = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
        dm.enqueue(r);
    }
    public  boolean isStoragePermissionGranted(int requestCode) {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                return true;
            } else {
                ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, requestCode);
                return false;
            }
        }
        else {
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            if(requestCode == 0) {
                new DownloadFile().execute(bookUri.toString());
            }
            else if(requestCode == 1) {
                downloadFileForViewing(bookUri,nameOfBook);
            }
        }
        else {
            Toast.makeText(this,"Please give permissions to write to external storage",Toast.LENGTH_SHORT).show();
        }
    }

    private class DownloadFile  extends AsyncTask<String, Integer, String> {
    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        // Create progress dialog
        mProgressDialog = new ProgressDialog(DisplayBooksActivity.this);
        // Set your progress dialog Title
        mProgressDialog.setTitle("Downloading");
        // Set your progress dialog Message
        mProgressDialog.setMessage("Downloading, Please Wait!");
        mProgressDialog.setIndeterminate(false);
        mProgressDialog.setMax(100);
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        // Show progress dialog
        mProgressDialog.show();
    }
    @Override
    protected String doInBackground(String... Url) {
        try {
            URL url = new URL(Url[0]);
            URLConnection connection = url.openConnection();
            connection.connect();
            // Detect the file lenghth
            int fileLength = connection.getContentLength();
            // Locate storage location
            String filepath = Environment.getExternalStorageDirectory()
                    .getPath();
            // Download the file
            InputStream input = new BufferedInputStream(url.openStream());
            // Save the downloaded file
            OutputStream output = new FileOutputStream(filepath+ "/"
                    + "jntucea.pdf");
            byte data[] = new byte[1024];
            long total = 0;
            int count;
            while ((count = input.read(data)) != -1) {
                total += count;
                // Publish the progress
                publishProgress((int) (total * 100 / fileLength));
                output.write(data, 0, count);
            }
            output.flush();
            output.close();
            input.close();
            Intent displayBook = new Intent(DisplayBooksActivity.this,PDFViewerActivity.class);
            displayBook.putExtra("link",url.toString());
            startActivity(displayBook);
        } catch (Exception e) {
            // Error Log
            Log.e("Error", e.getMessage());
            e.printStackTrace();
        }
        return null;
    }
    @Override
    protected void onProgressUpdate(Integer... progress) {
        super.onProgressUpdate(progress);
        mProgressDialog.setProgress(progress[0]);
    }

    @Override
    protected void onPostExecute(String s) {
        mProgressDialog.dismiss();
    }
}
}