package info.jntuacea.jntuacea;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class MainActivity extends AppCompatActivity {

    private Button submit,signOut;
    private FirebaseAuth.AuthStateListener authListener;
    private FirebaseAuth auth;
    private Spinner semesters_spinner,branches_spinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(getString(R.string.app_name));
        setSupportActionBar(toolbar);

        //get firebase auth instance
        auth = FirebaseAuth.getInstance();

        //get current user
        final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        authListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user == null) {
                    // user auth state is changed - user is null
                    // launch login activity
                    startActivity(new Intent(MainActivity.this, LoginActivity.class));
                    finish();
                }
            }
        };

        semesters_spinner = (Spinner) findViewById(R.id.semester_spinner);
        branches_spinner = (Spinner) findViewById(R.id.branches_spinner);

        semesters_spinner.setVisibility(View.GONE);

        branches_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(i != 0) {
                    semesters_spinner.setVisibility(View.VISIBLE);
                    Toast.makeText(MainActivity.this,"Please Select a Semester",Toast.LENGTH_SHORT).show();
                }
                else {
                    semesters_spinner.setVisibility(View.GONE);
                    Toast.makeText(MainActivity.this,"Please Select a Branch",Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });





        signOut = (Button) findViewById(R.id.sign_out);
        submit = (Button) findViewById(R.id.submit);

        signOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signOut();
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(branches_spinner.getSelectedItemPosition() == 0) {
                    Toast.makeText(MainActivity.this,"Please Select a Branch",Toast.LENGTH_SHORT).show();
                }
                else if(semesters_spinner.getSelectedItemPosition() == 0) {
                    Toast.makeText(MainActivity.this,"Please Select a Semester",Toast.LENGTH_SHORT).show();
                }
                else {
                    String branch = branches_spinner.getSelectedItem().toString();
                    String semester = semesters_spinner.getSelectedItem().toString();
                    Intent booksIntent = new Intent(MainActivity.this,DisplayBooksActivity.class);
                    booksIntent.putExtra("branch",branch);
                    booksIntent.putExtra("semester",semester);
                    startActivity(booksIntent);
                }
            }
        });

    }

    //sign out method
    public void signOut() {
        auth.signOut();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onStart() {
        super.onStart();
        auth.addAuthStateListener(authListener);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (authListener != null) {
            auth.removeAuthStateListener(authListener);
        }
    }
}